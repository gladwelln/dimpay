@extends('dimpay::layouts.app')

@section('content')

<div class="col-md-12 col-sm-12 p-tb20 p-lr10">
    <h2 class="text-uppercase m-b30">Send a transaction</h2>
    <div class="dez-tabs bg-tabs">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#transfer-content">
                    <i class="fa fa-send"></i>
                    <span class="title-head">Transfer</span>
                </a>
            </li>
            <li>
                <a data-toggle="tab" href="#mosaic-transfer-content">
                    <i class="fa fa-mutliple"></i>
                    <span class="title-head">Mosaic Transfer</span>
                </a>
            </li>
        </ul>
        <div class="tab-content bg-white p-lr10">
            <div id="transfer-content" class="tab-pane active">
                <form method="post" class="transfer-form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group"> 
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input name="recipient" id="recipient" type="text" class="form-control" placeholder="Recipient address">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                    <input name="amount" id="amount" onkeyup="getTransactionFee()" type="text" value="0" class="form-control" placeholder="Amount">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group"> 
                                    <span class="input-group-addon v-align-t"><i class="fa fa-clipboard"></i></span>
                                    <textarea name="message" onkeyup="getTransactionFee()" id="message" rows="4" class="form-control" placeholder="Your Message" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                    <input name="fee" id="fee" type="text" class="form-control" value="0.050000 XEM" readOnly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-angellist"></i></span>
                                    <input name="privateKey" id="privateKey" type="password" class="form-control" placeholder="Your private key">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-5">
                            <div class="checkbox">
                                <input id="isMultisig" type="checkbox" onchange="show_hide_multisig_content(this)" />
                                <label for="isMultisig">Is multisig transaction?</label>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group" id="multisigContent" style="display: none;">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-angellist"></i></span>
                                    <input name="multisigPublicKey" id="multisigPublicKey" type="password" class="form-control" placeholder="Multisig public key">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button name="submit" type="button" class="site-button" onclick="sendTransaction()"> <span>Submit</span> </button>
                            <button name="Resat" type="reset" class="site-button m-l30"> <span>Reset</span> </button>
                        </div>
                    </div>
                </form>
            </div>
            <div id="mosaic-transfer-content" class="tab-pane">
                <form method="post" class="mosaic-transfer-form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group"> 
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input name="recipient" id="recipient" type="text" class="form-control" placeholder="Recipient address">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group"> 
                                    <span class="input-group-addon"><i class="fa fa-bomb"></i></span>
                                    <input name="namespaceId" id="namespaceId" type="text" class="form-control" placeholder="Namespace ID">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                    <input name="amount" id="amount" readonly onkeyup="getMosaicTransactionFee(false)" type="text" value="1" class="form-control" placeholder="Amount">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group"> 
                                    <span class="input-group-addon"><i class="fa fa-bomb"></i></span>
                                    <input name="mosaicName" id="mosaicName" type="text" class="form-control" placeholder="Mosaic name">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group"> 
                                    <span class="input-group-addon v-align-t"><i class="fa fa-clipboard"></i></span>
                                    <textarea name="message" onkeyup="getMosaicTransactionFee(false)" id="message" rows="4" class="form-control" placeholder="Your Message" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group"> 
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                    <input name="mosaicAmount" id="mosaicAmount" type="text" class="form-control" placeholder="Mosaic amount">
                                    <span class="input-group-addon">
                                        <a href="javascript:;" onclick="getMosaicTransactionFee(true)"><span>Attach Mosaic</span></a>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                    <input name="fee" id="fee" type="text" class="form-control" value="0.050000 XEM" readOnly>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-angellist"></i></span>
                                    <input name="privateKey" id="privateKey" type="password" class="form-control" placeholder="Your private key">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="checkbox">
                                <input id="isMultisig2" type="checkbox" onchange="show_hide_multisig_content(this)" />
                                <label for="isMultisig2">Is multisig transaction?</label>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group" id="multisigContent" style="display: none;">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-angellist"></i></span>
                                    <input name="multisigPublicKey" id="multisigPublicKey" type="password" class="form-control" placeholder="Multisig public key">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button name="submit" type="button" class="site-button" onclick="sendMosaicTransaction()"> <span>Submit</span> </button>
                            <button name="Resat" type="reset" class="site-button m-l30"> <span>Reset</span> </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')

{!! Html::script(url('vendor/dimpay/js/transaction.js')) !!}

@endpush