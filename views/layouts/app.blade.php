<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicons Icon -->
    <link rel="icon" href="{!! asset('vendor/dimpay/img/favicon.ico') !!}" type="image/x-icon" />
    <link rel="shortcut icon" type="image/x-icon" href="{!! asset('vendor/dimpay/img/favicon.png') !!}" />

    <title>{!! $title or config('app.name', 'NEM\'s Dimpay') !!}</title>

    <!-- Styles -->
    {!! Html::style(url('vendor/dimpay/css/bootstrap.min.css')) !!}
    {!! Html::style(url('vendor/dimpay/css/font-awesome.min.css')) !!}
    {!! Html::style(url('vendor/dimpay/css/toastr.css')) !!}
    {!! Html::style(url('vendor/dimpay/css/sweetalert.css')) !!}
    {!! Html::style(url('vendor/dimpay/css/style.css')) !!}
    {!! Html::style(url('vendor/dimpay/css/skin.css')) !!}

    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
    
    @stack('style')

    <style>
        #imagecontainer 
        {
            background: url("{!! asset('vendor/dimpay/img/bg.jpg') !!}") no-repeat;
            width: 480px;
            height: 270px;    
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    {!! Html::script(url('vendor/dimpay/js/jquery-3.1.0.js')) !!}
    {!! Html::script(url('vendor/dimpay/js/jquery-blockui.js')) !!}
    {!! Html::script(url('vendor/dimpay/js/bootstrap.min.js')) !!}
    {!! Html::script(url('vendor/dimpay/js/jquery-ui.js')) !!}
    {!! Html::script(url('vendor/dimpay/js/toastr.min.js')) !!}
    {!! Html::script(url('vendor/dimpay/js/core.js')) !!}
    {!! Html::script(url('vendor/dimpay/js/nem-sdk.js')) !!}

    <script type="text/javascript">

        //Global functions
        function loading(action)
        {
            if(action == 'start')
            {
                $.blockUI({message: '<img src="{!! asset('vendor/dimpay/img/loader.gif') !!}">', css: { backgroundColor: 'transparent', border: 'none', cursor: 'wait' }, baseZ: 999999999});
            }
            else
            {
                $.unblockUI();    
            }
        }

        window.onbeforeunload = function(e)
        {
            loading('start');
        };

        //Global viriables
        var base_url = "{!! url('') !!}";
        var _token = "{{ csrf_token() }}";
        
        //NEM library
        var nem = require("nem-sdk").default;
        var activeNode = '{!! env('DEFAULT_NODE', 'testnet') !!}';
        var defaultNode = (activeNode == 'mainnet') ? nem.model.nodes.defaultMainnet : nem.model.nodes.defaultTestnet;
        var defaultPort = nem.model.nodes.defaultPort;
        var netId = (activeNode == 'mainnet') ? nem.model.network.data.mainnet.id : nem.model.network.data.testnet.id;

    </script>
</head>
<body>
    
    <div class="page-wraper"> 
        <div class="blog-page-conent style-">
            <div id="mySidenav" class="sidenav">
                <div class="logo-header mostion"><a href="{!! url('/') !!}">
                    <img src="{!! asset('vendor/dimpay/img/logo.svg') !!}" width="193" height="89" alt="NEM logo"></a>
                </div>
                <a href="javascript:void(0)" class="closebtn bg-primary">&times;</a>
                <div class="clearfix"></div>
                <ul class="p-b100">
                    <li>
                        <a href="{!! url('/') !!}" class="{!! (Route::getCurrentRoute()->uri() == '/') ? 'bg-primary' : '' !!}">
                            <i class="fa fa-home"></i>&nbsp;Dashboard
                        </a>
                    </li>
                    <li>
                        <a href="{!! url('quick-transfer') !!}" class="{!! (Route::getCurrentRoute()->uri() == 'quick-transfer') ? 'bg-primary' : '' !!}">
                            <i class="fa fa-send"></i>&nbsp;Quick Send
                        </a>
                    </li>
                    <li>
                        <a href="{!! url('bulk-transfer') !!}" class="{!! (Route::getCurrentRoute()->uri() == 'bulk-transfer') ? 'bg-primary' : '' !!}">
                            <i class="fa fa-upload"></i>&nbsp;Bulk Send
                        </a>
                    </li>
                </ul>
                <div class="left-footer-menu p-b10">
                    <ul class="list-inline footer-social text-center m-b0">
                        <li><a href="javascript:void(0);" class="fa fa-facebook bg-primary"></a></li>
                        <li><a href="javascript:void(0);" class="fa fa-twitter bg-primary"></a></li>
                        <li><a href="javascript:void(0);" class="fa fa-linkedin bg-primary"></a></li>
                        <li><a href="javascript:void(0);" class="fa fa-google-plus bg-primary"></a></li>
                    </ul>
                </div>
            </div>
            <span class="button-side-nav bg-primary openbtn"><i class="fa fa-bars"></i></span>
            
            <div class="clearfix">
                @yield('content')
            </div>
            
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="dimpay-global-mdl">
        {{--global modal--}}
    </div>

    @stack('scripts')

    @if(Session::has('flash_error'))

        <script type="text/javascript">
            notify('error', '{!! Session::get('flash_error') !!}', '', 0);
        </script>

    @endif

    @if(Session::has('flash_warning'))

        <script type="text/javascript">
            notify('warning', '{!! Session::get('flash_warning') !!}', '', 0);
        </script>

    @endif

    @if(Session::has('flash_success'))

        <script type="text/javascript">
            notify('success', '{!! Session::get('flash_success') !!}');
        </script>

    @endif

    @if(Session::has('flash_info'))

        <script type="text/javascript">
            notify('info', '{!! Session::get('flash_info') !!}', '', 0);
        </script>

    @endif

</body>
</html>