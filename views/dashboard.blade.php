@extends('dimpay::layouts.app')

@section('content')
    
    <div class="col-md-12 col-sm-12 p-tb20 p-lr10">
        <h3 class="text-uppercase">
        transaction History <a class="site-button" href="{!! url('clear-transactions') !!}">Clear</a>
        </h2>
        <table class="table table-responsive table-hover bg-white" style="font-size:small">
            <thead>
                <tr>
                    <th width="15%">Alias</th>
                    <th width="20%">Address</th>
                    <th width="10%" class="text-center">Amount</th>
                    <th width="15%">Message</th>
                    <th width="20%">Hash</th>
                    <th width="20%">Date</th>
                </tr>
            </thead>
            <tbody>
                @if(count($transactions))
                    @foreach($transactions as $transaction)
                        <tr class="{!! ($transaction->status) ? 'bg-success' : 'bg-danger' !!}">
                            <td>{!! $transaction->alias !!}</td>
                            <td>{!! $transaction->address !!}</td>
                            <td class="text-center">{!! $transaction->amount !!}</td>
                            <td>{!! $transaction->message !!}</td>
                            <td>{!! $transaction->hash !!}</td>
                            <td>{!! $transaction->created_at !!}</td>
                        </tr>
                    @endforeach
                @else
                <tr>
                    <td colspan="6">
                        <div class="alert alert-info">
                            <strong><i class="fa fa-info-circle"></i> Info:</strong> There are no transaction records, click <a href="{!! url('quick-transfer') !!}">Quick Send</a> or <a href="{!! url('bulk-transfer') !!}">Bulk Send</a> to transfer.
                        </div>
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
        <div class="pull-right">
            {!! $transactions->links() !!}
        </div>
    </div> 

@endsection