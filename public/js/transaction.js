//Create an NIS endpoint object
function getEndpoint()
{
    return nem.model.objects.create("endpoint")(defaultNode, defaultPort);
}

//Create an NIS transferTransaction / common / mosaicDefinitionMetaDataPair object
function getObject(which)
{
    return nem.model.objects.get(which);
}

//For XEM transfers
function getTransactionFee()
{
    var amount = $("form.transfer-form #amount").val();
    var message = $("form.transfer-form #message").val();

    if(!amount || !nem.utils.helpers.isTextAmountValid(amount)) return notify("error", 'Please input a valid amount to continue!', '', 5000);

    var fee = calculateFee(amount, message, getObject('transferTransaction'));
    var feeString = nem.utils.format.nemValue(fee)[0] + "." + nem.utils.format.nemValue(fee)[1];

    //Set fee in view
    $("form.transfer-form #fee").val(feeString + ' XEM');
}

//For Mosaic transfers
async function getMosaicTransactionFee(isMosaic) 
{
    var amount = $("form.mosaic-transfer-form #amount").val();
    var message = $("form.mosaic-transfer-form #message").val();
    var namespaceId = $("form.mosaic-transfer-form #namespaceId").val();
    var mosaicName = $("form.mosaic-transfer-form #mosaicName").val();
    var mosaicAmount = $("form.mosaic-transfer-form #mosaicAmount").val();

    // Check for amount errors
    if(!amount || !nem.utils.helpers.isTextAmountValid(amount)) return notify("error", 'Please input a valid amount to continue!', '', 5000);
    if(isMosaic)
    {
        if(!namespaceId) return notify("error", 'Please input a namespace Id to continue!', '', 5000);
        if(!mosaicName) return notify("error", 'Please input a mosaic name to continue!', '', 5000);
        if(!mosaicAmount || !nem.utils.helpers.isTextAmountValid(mosaicAmount)) return notify("error", 'Please input a valid mosiac amount to continue!', '', 5000);
    }

    //Get the transaction object
    var transferTransaction = getObject('transferTransaction');

    if(namespaceId && mosaicName && mosaicAmount)
    {
        var res = await getMosaicInfo(namespaceId, mosaicName, mosaicAmount, transferTransaction);
        if(res.status == false)
        {
            return notify('error', res.error_description);
        }
        //Override the transaction object with the newly created
        transferTransaction = res.payload;
    }

    //Calculate the fee
    var fee = calculateFee(amount, message, transferTransaction);
    
    var feeString = nem.utils.format.nemValue(fee)[0] + "." + nem.utils.format.nemValue(fee)[1];

    //Set fee in view
    $("form.mosaic-transfer-form #fee").val(feeString + ' XEM');

    if(isMosaic)
    {
        // Calculate back the quantity to an amount to show as notification. It should be the same as user input but we double check to see if quantity is correct.
        var totalToShow = nem.utils.format.supply(transferTransaction.mosaics[0].quantity, {"namespaceId": namespaceId, "name": mosaicName}, transferTransaction.mosaicDefinitionMetaDataPair)[0] + '.' + nem.utils.format.supply(transferTransaction.mosaics[0].quantity, {"namespaceId": namespaceId, "name": mosaicName}, transferTransaction.mosaicDefinitionMetaDataPair)[1];

        notify('info', "You are about to send " + totalToShow + " " + namespaceId + ":" + mosaicName, "Good job!", 7000);
    }
}

function calculateFee(amount, message, transferTransaction) 
{
    // Set the cleaned amount into transfer transaction object
    transferTransaction.amount = nem.utils.helpers.cleanTextAmount(amount);

    // Set the message into transfer transaction object
    transferTransaction.message = message;

    var transactionEntity;
    var hasMosaics = (transferTransaction.mosaics.length) ? true : false;
    var common = getObject('common');

    //Prepare the updated transfer transaction object
    if(hasMosaics)
    {
        transactionEntity = nem.model.transactions.prepare("mosaicTransferTransaction")(common, transferTransaction, transferTransaction.mosaicDefinitionMetaDataPair, netId);
    }
    else
    {
        transactionEntity = nem.model.transactions.prepare("transferTransaction")(common, transferTransaction, netId);
    }
    
    return transactionEntity.fee;
}

async function getMosaicInfo(namespaceId, mosaicName, mosaicAmount, transferTransaction)
{
    var mosaicDefinitionMetaDataPair = getObject('mosaicDefinitionMetaDataPair');
    var endpoint = getEndpoint();

    // If not XEM, fetch the mosaic definition from network
    if(mosaicName !== 'xem') 
    {
        var res = await nem.com.requests.namespace.mosaicDefinitions(endpoint, namespaceId);
        
        // Look for the mosaic definition(s) we want in the request response (Could use ["eur", "usd"] to return eur and usd mosaicDefinitionMetaDataPairs)
        var neededDefinition = nem.utils.helpers.searchMosaicDefinitionArray(res.data, [mosaicName]);
        
        // Get full name of mosaic to use as object key
        var fullMosaicName  = namespaceId + ':' + mosaicName;
        var mosaicDefinition = neededDefinition[fullMosaicName];
        
        // Check if the mosaic was found
        if(!mosaicDefinition) return { status: false, error_description: 'Mosaic not found, try again!' };
        
        // Set mosaic definition into mosaicDefinitionMetaDataPair
        mosaicDefinitionMetaDataPair[fullMosaicName] = {};
        mosaicDefinitionMetaDataPair[fullMosaicName].mosaicDefinition = mosaicDefinition;
        mosaicDefinitionMetaDataPair[fullMosaicName].supply = mosaicDefinition.properties[1].value;

        //Add it to the transaction object in order to calculate fees
        transferTransaction.mosaicDefinitionMetaDataPair = mosaicDefinitionMetaDataPair;

        // Now we have the definition we can calculate quantity out of user input
        var quantity = nem.utils.helpers.cleanTextAmount(mosaicAmount) * Math.pow(10, neededDefinition[fullMosaicName].properties[0].value);
        
        // Create a mosaic attachment
        var mosaicAttachment = nem.model.objects.create("mosaicAttachment")(namespaceId, mosaicName, quantity);

        // Push attachment into transaction mosaics
        transferTransaction.mosaics.push(mosaicAttachment);
    }
    else
    {
        // Calculate quantity from user input, XEM divisibility is 6
        var quantity = nem.utils.helpers.cleanTextAmount(mosaicAmount) * Math.pow(10, 6);

        // Create a mosaic attachment
        var mosaicAttachment = nem.model.objects.create("mosaicAttachment")(namespaceId, mosaicName, quantity);

        // Push attachment into transaction mosaics
        transferTransaction.mosaics.push(mosaicAttachment);

        //Add it to the transaction object in order to calculate fees
        transferTransaction.mosaicDefinitionMetaDataPair = mosaicDefinitionMetaDataPair;
    }

    return { status: true, payload: transferTransaction };
}

async function send(transaction)
{
    var recipient = transaction.recipient;
    var amount = transaction.amount;
    var privateKey = transaction.privateKey;
    var message = transaction.message;
    var multisigPublicKey = transaction.multisigPublicKey;
    var isMultisig = transaction.isMultisig;
    var mosaics = transaction.mosaics;
    var mosaicDefinitionMetaDataPair = transaction.mosaicDefinitionMetaDataPair;
    var results;
    var transactionEntity;

    //Get objects
    var endpoint = getEndpoint();
    var common = getObject('common');
    var transferTransaction = getObject('transferTransaction');

    // Set the private key in common object
    common.privateKey = privateKey;

    //Set multisig flags
    if(isMultisig)
    {
        transferTransaction.isMultisig = true;
        transferTransaction.multisigAccount = { 'publicKey': multisigPublicKey };
    }

    // Set the cleaned amount into transfer transaction object
    transferTransaction.amount = nem.utils.helpers.cleanTextAmount(amount);

    // Recipient address must be clean (no hypens: "-")
    transferTransaction.recipient = nem.model.address.clean(recipient);

    // Set message
    transferTransaction.message = message;

    // Prepare the updated transfer transaction object
    if(mosaics && mosaics.length)
    {
        transferTransaction.mosaics = mosaics;
        transactionEntity = nem.model.transactions.prepare("mosaicTransferTransaction")(common, transferTransaction, mosaicDefinitionMetaDataPair, netId);
    }
    else
    {
        transactionEntity = nem.model.transactions.prepare("transferTransaction")(common, transferTransaction, netId);
    }
    
    var res;
    try
    {
        // Serialize transfer transaction and announce
        res = await nem.model.transactions.send(common, transactionEntity, endpoint);

        //If code >= 2, it's an error
        if(res.code >= 2)
        {
            res = { status: false, error_description: res.message };
        }
        else 
        {
            res = { status: true, payload: res };
        }
    }
    catch(e)
    {
        // console.log("in here", e);
        res = { status: false, error_description: (e.data.message) ? e.data.message : 'Intenal server error, please try again!' };
    }    
    
    return res;
}

async function sendTransaction()
{
    var recipient = $("form.transfer-form #recipient").val();
    var amount = $("form.transfer-form #amount").val();
    var privateKey = $("form.transfer-form #privateKey").val();
    var message = $("form.transfer-form #message").val();
    var multisigPublicKey = $('form.transfer-form #multisigPublicKey').val();
    var isMultisig = $('form.transfer-form #isMultisig').prop('checked');

    //Check form for errors
    if(!recipient) return notify("error", 'Please input a recipient to continue!', '', 5000);
    if(!amount || !nem.utils.helpers.isTextAmountValid(amount)) return notify("error", 'Please input a valid amount to continue!', '', 5000);
    if(!privateKey) return notify("error", 'Please input a private key to continue!', '', 5000);
    if(isMultisig && !multisigPublicKey) return notify("error", 'Please input a multisig public key to continue!', '', 5000);
    if (!nem.model.address.isValid(nem.model.address.clean(recipient))) return notify("error", 'Invalid recipent address!', '', 5000);
    // Check private key for errors
    if (privateKey.length !== 64 && privateKey.length !== 66) return notify("error", 'Invalid private key, length must be 64 or 66 characters !', '', 5000);
    if (!nem.utils.helpers.isHexadecimal(privateKey)) return notify("error", 'Private key must be hexadecimal only !', '', 5000);

    loading('start');

    var transaction = {
        'recipient': recipient, 
        'amount': amount, 
        'message': message, 
        'privateKey': privateKey, 
        'isMultisig': isMultisig,
        'multisigPublicKey': multisigPublicKey
    };

    var transactionInfo = { recipient: recipient, amount: amount, alias: 'quicksend:xem' };
    
    var res = await send(transaction);
    if(res.status)
    {
        transactionInfo.message = res.payload.message;
        transactionInfo.hash = res.payload.transactionHash.data;

        notify('success', 'Transaction successful!', '', 5000);
    }
    else
    {
        transactionInfo.message = res.error_description;

        notify("error", res.error_description);
    }

    transactionInfo.status = (res.status) ? 1 : 0;

    saveTransactionDetails(transactionInfo);

    loading('stop');
}

async function sendMosaicTransaction()
{
    var privateKey = $("form.mosaic-transfer-form #privateKey").val();
    var recipient = $("form.mosaic-transfer-form #recipient").val();
    var amount = $("form.mosaic-transfer-form #amount").val();
    var message = $("form.mosaic-transfer-form #message").val();
    var multisigPublicKey = $('form.mosaic-transfer-form').find('#multisigPublicKey').val();
    var isMultisig = $('form.mosaic-transfer-form #isMultisig2').prop('checked');
    
    var namespaceId = $("form.mosaic-transfer-form #namespaceId").val();
    var mosaicName = $("form.mosaic-transfer-form #mosaicName").val();
    var mosaicAmount = $("form.mosaic-transfer-form #mosaicAmount").val();
    
    //Check form for errors
    if(!recipient) return notify("error", 'Please input a recipient to continue!', '', 5000);
    if(!privateKey) return notify("error", 'Please input a private key to continue!', '', 5000);
    if(isMultisig && !multisigPublicKey) return notify("error", 'Please input a multisig public key to continue!', '', 5000);
    if (!nem.model.address.isValid(nem.model.address.clean(recipient))) return notify("error", 'Invalid recipent address!', '', 5000);
    // Check private key for errors
    if (privateKey.length !== 64 && privateKey.length !== 66) return notify("error", 'Invalid private key, length must be 64 or 66 characters !', '', 5000);
    if (!nem.utils.helpers.isHexadecimal(privateKey)) return notify("error", 'Private key must be hexadecimal only !', '', 5000);
    //Check mosaic
    if(!namespaceId) return notify("error", 'Please input a namespace Id to continue!', '', 5000);
    if(!mosaicName) return notify("error", 'Please input a mosaic name to continue!', '', 5000);
    if(!mosaicAmount || !nem.utils.helpers.isTextAmountValid(mosaicAmount)) return notify("error", 'Please input a valid mosiac amount to continue!', '', 5000);

    // Create a mosaic attachment
    var res = await getMosaicInfo(namespaceId, mosaicName, mosaicAmount, getObject('transferTransaction'));
    if(res.status == false)
    {
        return notify('error', res.error_description);
    }

    loading('start');

    var transaction = {
        'recipient': recipient, 
        'amount': amount, 
        'message': message, 
        'privateKey': privateKey, 
        'isMultisig': isMultisig,
        'multisigPublicKey': multisigPublicKey,
        'mosaics': res.payload.mosaics,
        'mosaicDefinitionMetaDataPair': res.payload.mosaicDefinitionMetaDataPair
    };

    var transactionInfo = { recipient: recipient, amount: mosaicAmount, alias: 'quicksend:' + namespaceId + ":" + mosaicName };

    var res = await send(transaction);
    if(res.status)
    {
        transactionInfo.message = res.payload.message;
        transactionInfo.hash = res.payload.transactionHash.data;

        notify('success', 'Transaction successful!', '', 5000);
    }
    else
    {
        transactionInfo.message = res.error_description;

        notify("error", res.error_description);
    }

    transactionInfo.status = (res.status) ? 1 : 0;

    saveTransactionDetails(transactionInfo);

    loading('stop');
}

function show_hide_multisig_content(obj)
{
    var div = $('.tab-pane.active').find('#multisigContent');
    if(obj.checked)
    {
        div.fadeIn('slow');
    }
    else
    {
        div.fadeOut('slow');
    }
}

function saveTransactionDetails(transaction)
{
    $.doAJAX(base_url + '/ajax_save_transaction_details', { transaction: transaction }, 'POST', function (response)
    {
        if (response.status == true)
        {
            var content = (transaction.status) ? '<li class="list-group-item list-group-item-success">' + transaction.hash + '</li>' : '<li class="list-group-item list-group-item-danger">' + transaction.message + '</li>';
            $('ul#transactionPlaceholder').prepend(content);
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

async function sendBulkTransactions()
{
    var recipients = $("form.transfer-form #recipients").val();
    var message = $("form.transfer-form #message").val();
    var multisigPublicKey = $('form.transfer-form #multisigPublicKey').val();
    var isMultisig = $('form.transfer-form #isMultisig').prop('checked');
    var privateKey = $("form.transfer-form #privateKey").val();

    //Check form for errors
    if(!recipients) return notify("error", 'Please upload at-least one recipient to continue!', '', 5000);
    
    var data;
    var errors = "";
    var lines = recipients.split('\n');
    for(var line = 0; line < lines.length; line++)
    {
        data = lines[line].split(',');

        if (!data[1] || !nem.model.address.isValid(nem.model.address.clean(data[1])))
        {
            errors += "Invalid recipent address in line " + (line + 1) + "<br>";
        }

        if(!data[0] || !nem.utils.helpers.isTextAmountValid(data[0]))
        {
            errors += "Invalid amount in line " + (line + 1) + "<br>";
        }
    }
    
    if(errors.length)
    {
        return notify("error", errors);
    }

    if(!privateKey) return notify("error", 'Please input a private key to continue!', '', 5000);
    if(isMultisig && !multisigPublicKey) return notify("error", 'Please input a multisig public key to continue!', '', 5000);
    
    // Check private key for errors
    if (privateKey.length !== 64 && privateKey.length !== 66) return notify("error", 'Invalid private key, length must be 64 or 66 characters !', '', 5000);
    if (!nem.utils.helpers.isHexadecimal(privateKey)) return notify("error", 'Private key must be hexadecimal only !', '', 5000);
    
    loading('start');
    
    for(var line = 0; line < lines.length; line++)
    {
        data = lines[line].split(',');

        var transaction = {
            'recipient': data[1],
            'amount': data[0],
            'message': message, 
            'privateKey': privateKey, 
            'isMultisig': isMultisig,
            'multisigPublicKey': multisigPublicKey
        };
        
        var transactionInfo = { recipient: data[1], amount: data[0], alias: 'bulksend:xem' };

        var res = await send(transaction);
        if(res.status)
        {
            transactionInfo.message = res.payload.message;
            transactionInfo.hash = res.payload.transactionHash.data;

            notify('success', 'Transaction successful!', '', 8000);
        }
        else
        {
            transactionInfo.message = res.error_description;

            notify('error', res.error_description, '', 8000);
        }

        transactionInfo.status = (res.status) ? 1 : 0;

        saveTransactionDetails(transactionInfo);
    }

    loading('stop');
}

async function sendBulkMosaicTransactions()
{
    var privateKey = $("form.mosaic-transfer-form #privateKey").val();
    var recipients = $("form.mosaic-transfer-form #recipients").val();
    var amount = $("form.mosaic-transfer-form #amount").val();
    var message = $("form.mosaic-transfer-form #message").val();
    var multisigPublicKey = $('form.mosaic-transfer-form').find('#multisigPublicKey').val();
    var isMultisig = $('form.mosaic-transfer-form #isMultisig2').prop('checked');
    
    var namespaceId = $("form.mosaic-transfer-form #namespaceId").val();
    var mosaicName = $("form.mosaic-transfer-form #mosaicName").val();
    
    //Check form for errors
    if(!recipients) return notify("error", 'Please upload at-least one recipient to continue!', '', 5000);
    
    var data;
    var errors = "";
    var lines = recipients.split('\n');
    for(var line = 0; line < lines.length; line++)
    {
        data = lines[line].split(',');

        if (!data[1] || !nem.model.address.isValid(nem.model.address.clean(data[1])))
        {
            errors += "Invalid recipent address in line " + (line + 1) + "<br>";
        }

        if(!data[0] || !nem.utils.helpers.isTextAmountValid(data[0]))
        {
            errors += "Invalid mosaic amount in line " + (line + 1) + "<br>";
        }
    }
    
    if(errors.length)
    {
        return notify("error", errors);
    }

    if(!privateKey) return notify("error", 'Please input a private key to continue!', '', 5000);
    if(isMultisig && !multisigPublicKey) return notify("error", 'Please input a multisig public key to continue!', '', 5000);
    // Check private key for errors
    if (privateKey.length !== 64 && privateKey.length !== 66) return notify("error", 'Invalid private key, length must be 64 or 66 characters !', '', 5000);
    if (!nem.utils.helpers.isHexadecimal(privateKey)) return notify("error", 'Private key must be hexadecimal only !', '', 5000);
    //Check mosaic
    if(!namespaceId) return notify("error", 'Please input a namespace Id to continue!', '', 5000);
    if(!mosaicName) return notify("error", 'Please input a mosaic name to continue!', '', 5000);
    
    loading('start');

    for(var line = 0; line < lines.length; line++)
    {
        data = lines[line].split(',');

        var transactionInfo = { recipient: data[1], amount: data[0], alias: "bulksend:" + namespaceId + ":" + mosaicName };

        // Create a mosaic attachment
        var mosaic_res = await getMosaicInfo(namespaceId, mosaicName, data[0], getObject('transferTransaction'));
        if(mosaic_res.status == false)
        {
            transactionInfo.status = 0;
            transactionInfo.message = mosaic_res.error_description;
        }
        else
        {        
            var transaction = {
                'recipient': data[1], 
                'amount': data[0], 
                'message': message, 
                'privateKey': privateKey, 
                'isMultisig': isMultisig,
                'multisigPublicKey': multisigPublicKey,
                'mosaics': mosaic_res.payload.mosaics,
                'mosaicDefinitionMetaDataPair': mosaic_res.payload.mosaicDefinitionMetaDataPair
            };

            var res = await send(transaction);
            if(res.status)
            {
                transactionInfo.message = res.payload.message;
                transactionInfo.hash = res.payload.transactionHash.data;

                notify('success', 'Transaction successful!', '', 8000);
            }
            else
            {
                transactionInfo.message = res.error_description;

                notify('error', res.error_description, '', 8000);
            }

            transactionInfo.status = (res.status) ? 1 : 0;
        }

        saveTransactionDetails(transactionInfo);
    }

    loading('stop');
}

function loadFileData(obj, form_class)
{
    loading('start');

    var file = obj.files[0];
    var reader = new FileReader();
    
    reader.onload = function(progressEvent)
    {
        var lines = this.result.split('\n');
        for(var line = 0; line < lines.length; line++)
        {
            if(line)
            {
                var tmp_val = ($("form." + form_class + " #recipients").val()) ? $("form." + form_class + " #recipients").val() + '\n' + lines[line] : lines[line];
                $("form." + form_class + " #recipients").val(tmp_val.trim());
            }
        }
    };

    reader.readAsText(file);

    loading('stop');
}