jQuery.extend({
    doAJAX: function (url, data, type, callback)
    {
        if (type.toLowerCase() != "get")
        {
            data["_token"] = _token;
        }

        type = !(type) ? "GET" : type;

        return $.ajax({
            type: type,
            url: url,
            data: data,
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                notify('error',"Error Code: " + XMLHttpRequest.status + " : " + XMLHttpRequest.statusText);
            },
            success: function (data)
            {
                callback(data);
            }
        });
    }
});

function notify(type, msg, title, time_out)
{
    //toastr.clear();

    time_out = (time_out == undefined) ? 0 : time_out;

    toastr.options = {
        "closeButton": true,
        "closeHtml": "<i class='glyphicon glyphicon-remove-sign'></i>",
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": 300,
        "hideDuration": 300,
        "timeOut": time_out,
        "extendedTimeOut": 0,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    toastr[type](msg, title);
}

function openNav()
{	
	$('.openbtn').on('click',function(e)
	{
		e.preventDefault();
		if($('#mySidenav').length > 0)
		{
			document.getElementById("mySidenav").style.left = "0";
		}	
	});
}

function closeNav()
{	
	$('.closebtn').on('click',function(e)
	{
		e.preventDefault();
		if($('#mySidenav').length > 0)
		{
			document.getElementById("mySidenav").style.left = "-300px";
		}
	});
}

$(document).ready(function()
{
	closeNav();
	openNav();
});