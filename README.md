# Laravel NEM transactions
A laravel package to do transactions using the NEM blockchain.

## Installation

[PHP](https://php.net) 7+, [Laravel](https://laravel.com/docs/5.4) 5.4+ and [Composer](https://getcomposer.org) are required.

To get the latest version of Laravel, and simply add the following to your `composer.json` file.

```
"repositories": [
  	{
        "type": "git",
    	"url": "https://gladwelln@bitbucket.org/gladwelln/dimpay.git"
	}
],

"require": {
	"gladwelln/dimpay": "dev-master"
}
```

You'll then need to run `composer install` or `composer update` to download it and have the autoloader updated.
Also run `php artisan vendor:publish` to publish public files.

If you are using Laravel 5.5 or later, the service provider auto discovery feature will load the required classes after installation otherwise, you need to register the service provider.
Open up `config/app.php` and add the following to the `providers` key.

* `gladwelln\dimpay\DimpayServiceProvider::class`


## Configuration

You can switch to mainet / testnet from your .env file:

```bash
DEFAULT_NODE=testnet/mainnet
```

Update DB credentials on your .env file:

```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=dimpay
DB_USERNAME=root
DB_PASSWORD=******
```

Migrate transaction table:
```bash
php artisan migrate
```

Then you are ready to get running. Just visit and start sending transactions as per [NEM NIS API Documentation](https://nemproject.github.io/#initiating-a-transaction)

```bash
http://{{site-url}}/
```