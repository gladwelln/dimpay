<?php

namespace gladwelln\dimpay;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use App;
use DB;
use Log;

class DimpayServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(realpath(__DIR__ . '/../views'), 'dimpay');

        $this->setupRoutes($this->app->router);

        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        $this->publishes([
            __DIR__ . '/../public' => public_path('vendor/dimpay'),
        ], 'public');

        //Log all queries executed on the app
        DB::listen(function ($query)
        {
            //Log::info($query->sql, $query->bindings);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //Register service providers
        $this->app->register('Collective\Html\HtmlServiceProvider');

        //Load service provider aliases
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Html', 'Collective\Html\HtmlFacade');
        $loader->alias('Form', 'Collective\Html\FormFacade');
    }

    public function setupRoutes(Router $router)
    {
        if (!$this->app->routesAreCached())
        {
            require __DIR__.'/routes/web.php';
        }
    }
}