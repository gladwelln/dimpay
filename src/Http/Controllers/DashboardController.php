<?php

namespace gladwelln\dimpay\Http\Controllers;

use App\Http\Controllers\Controller;
use gladwelln\dimpay\Models\Transaction;

class DashboardController extends Controller
{
    public function __construct() { }

    public function index()
    {
        $transactions = Transaction::paginate(16);

        return view('dimpay::dashboard', compact('transactions'));
    }
}