<?php

namespace gladwelln\dimpay\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \gladwelln\dimpay\Models\Transaction;

use Response;
use DB;

class TransactionsController extends Controller
{
    public function __construct() { }

    public function quick_transfer()
    {
        return view('dimpay::transactions.quick-transfer-dashboard');
    }

    public function bulk_transfer()
    {
        return view('dimpay::transactions.bulk-transfer-dashboard');
    }

    public function save_transaction_details(Request $request)
    {
        $transaction = $request->get('transaction');
        $status = $transaction['status'];
        $recipient = $transaction['recipient'];
        $amount = $transaction['amount'];
        $alias = $transaction['alias'];
        $message = $transaction['message'];
        $hash = isset($transaction['hash']) ? $transaction['hash'] : '';

        $trans = new Transaction();
        $trans->alias = $alias;
        $trans->address = $recipient;
        $trans->amount = $amount;
        $trans->status = $status;
        $trans->message = $message;
        $trans->hash = $hash;
        $trans->created_at = DB::raw('CURRENT_TIMESTAMP');
        $trans->save();

        return Response::json(['status' => true]);
    }

    function clear_transaction_details()
    {
        $total = Transaction::count();

        Transaction::truncate();

        return redirect('/')->with(['flash_success' => "$total transactions successfully cleared!"]);
    }
}