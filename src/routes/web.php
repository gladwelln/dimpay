<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['namespace' => '\gladwelln\dimpay\Http\Controllers', 'middleware' => 'web'], function()
{
    Route::get('/', 'DashboardController@index');
    Route::get('quick-transfer', 'TransactionsController@quick_transfer');
    Route::get('bulk-transfer', 'TransactionsController@bulk_transfer');
    
    Route::post('ajax_save_transaction_details', 'TransactionsController@save_transaction_details');
    Route::get('clear-transactions', 'TransactionsController@clear_transaction_details');
});